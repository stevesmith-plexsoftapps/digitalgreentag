﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using digitalgreencard.ViewModels;
using digitalgreencard.Views;

namespace digitalgreencard
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            var isLoggedOn = "0";
            if (isLoggedOn == "1")
            {
                Application.Current.MainPage = new AppShell();
            }
            else
            {
                MainPage = new NavigationPage(new LoginPage());
                isLoggedOn = "1";
            }

            return;
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
